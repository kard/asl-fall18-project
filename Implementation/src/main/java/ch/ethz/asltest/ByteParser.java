
package ch.ethz.asltest;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.operations.CompoundMultiGetOperation;
import ch.ethz.asltest.operations.GetOperation;
import ch.ethz.asltest.operations.MultiGetOperation;
import ch.ethz.asltest.operations.Operation;
import ch.ethz.asltest.operations.SetOperation;

/**
 * This class implements a parser to parse the requests and responses in a byte level.
 * The thread that runs this parser reads data from the request and in case the data is incomplete,
 * it waits for the NetThread to complete the reading from the client. Afterwards, the Parser
 * resumes from the place it left, to parse the rest. If invalid request, the parser throws an
 * ParserException.
 * @author ardkastrati
 *
 */
public class ByteParser {
	
	private static final String PARSE_INPUT_EXCEPTION = "Expected a valid type of command, but found an unknown memcached command!";
	private static final Logger logger = LogManager.getLogger(ByteParser.class);
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	
	private boolean sharded;
	private int id;
	
	byte[] data;
	int position;
	
	/**
	 * During the parsing the next token is returned
	 * @author ardkastrati
	 *
	 */
	private enum Token {
		/**
		 * Next token is a word
		 */
		WORD,
		/**
		 * Next token is a white space
		 */
		SPACE,
		/**
		 * Next token is end of line.
		 */
		EOL
	}
	
	/**
	 * Constructs the parser
	 * @param id the id of the parser (the same as the id of the worker thread).
	 * @param sharded
	 */
	public ByteParser(int id, boolean sharded) {
		this.id = id;
		this.sharded = sharded;
		this.data = new byte[0];
		logger.debug(String.format("Parser with id %d is constructed.", id));
	}
	
	/**
	 * Parses and verifies the request. If the request incomplete, waits for the next data.
	 * If request verified and complete it creates an appropriate operation Object. In case the
	 * request invalid, ParserException is thrown.
	 * @param clientEndpoint
	 * @return Operation the parsed operation.
	 * @throws ParserException
	 * @throws InterruptedException
	 * @see Operation
	 */
	public Operation parseRequest(ClientEndpoint clientEndpoint) throws ParserException, InterruptedException {
		logger.debug(String.format("Parser with id %d is parsing a request with id %d.", id, clientEndpoint.getId()));
		Operation op;
		this.getNewData(clientEndpoint);
		logger.debug(String.format("Parser with id %d : Got data to parse with size is %d", id, data.length));
		op = startTokenParsing(clientEndpoint);
		clientEndpoint.clearRequest(); //data successfully parsed
		this.clearParser();
		return op;
	}
	
	
	private Operation startTokenParsing(ClientEndpoint clientEndpoint) throws ParserException, InterruptedException {
		Operation op;
		
		if(this.matchNextChars(clientEndpoint, 'g', 'e', 't', ' ')) {
			logger.debug(String.format("Parser with id %d : First token is get", id));
			position += 4;
			op = parseGetRequest(clientEndpoint);
		} else if(this.matchNextChars(clientEndpoint, 's', 'e', 't', ' ')) {
			logger.debug(String.format("Parser with id %d : First token is set", id));
			position += 4;
			op = parseSetRequest(clientEndpoint);
		} else {
			logger.debug(String.format("Parser with id %d : First token is WRONG", id));
			throw new ParserException(PARSE_INPUT_EXCEPTION);
		}
		return op;
	}

	private Operation parseGetRequest(ClientEndpoint clientEndpoint) throws ParserException, InterruptedException {
		Operation op;
		this.matchNextToken(clientEndpoint, Token.WORD); //read first key
		int next = this.matchNextToken(clientEndpoint, Token.EOL, Token.SPACE);
		if(next == 0) { //end of request
			logger.debug(String.format("Parser with id %d : GET : There is only one key.", id));
			op = new GetOperation(data);
			return op;
		} else { // more keys
			logger.debug(String.format("Parser with id %d : GET : There are multiple keys.", id));
			if(!sharded) {
				logger.debug(String.format("Parser with id %d : MULTIGET", id));
				int keys = 3;
				while(this.matchNextToken(clientEndpoint, Token.EOL, Token.WORD, Token.SPACE) != 0) keys++; //reading next keys.
				keys /= 2; //Double counted space and work
				op = new MultiGetOperation(data, keys);
			} else {
				List<Integer> whiteSpaces = new ArrayList<Integer>();
				whiteSpaces.add(3); //first whitespace
				whiteSpaces.add(this.position-1);
				this.matchNextToken(clientEndpoint, Token.WORD);
				while(this.matchNextToken(clientEndpoint, Token.SPACE, Token.EOL) == 0) {
					whiteSpaces.add(this.position-1);
					next = this.matchNextToken(clientEndpoint, Token.WORD);
					if(next == 1) break; 
				}
				whiteSpaces.add(this.position-2);
				logger.debug(String.format("Parser with id %d : GET : There are %d keys.", id, whiteSpaces.size() - 1));
				op = new CompoundMultiGetOperation(data, whiteSpaces);
			}
		}
		return op;
	}
	
	private Operation parseSetRequest(ClientEndpoint clientEndpoint) throws InterruptedException, ParserException {
		this.matchNextToken(clientEndpoint, Token.WORD); //key
		this.matchNextToken(clientEndpoint, Token.SPACE); //' '
		this.matchNextToken(clientEndpoint, Token.WORD); //flags
		this.matchNextToken(clientEndpoint, Token.SPACE); //' '
		this.matchNextToken(clientEndpoint, Token.WORD); //exptime
		this.matchNextToken(clientEndpoint, Token.SPACE); //' '
		this.matchNextToken(clientEndpoint, Token.WORD); //bytes
		this.matchNextToken(clientEndpoint, Token.EOL); //\r\n
		this.matchNextToken(clientEndpoint, Token.WORD); //data block
		this.matchNextToken(clientEndpoint, Token.EOL); //\r\n
		
		Operation op = new SetOperation(data);
		return op;
	}
	
	/**
	 * Tries to match the next token with one of the provided tokens.
	 * @param clientEndpoint
	 * @param tokens possible tokens to match.
	 * @throws ParserException
	 * @throws InterruptedException 
	 */
	private int matchNextToken(ClientEndpoint clientEndpoint, Token... tokens) throws ParserException, InterruptedException {
		Token next = this.readNextToken(clientEndpoint);
		for(int i = 0; i < tokens.length; i++) {
			if(next == tokens[i]) return i;
		}
		throw new ParserException(PARSE_INPUT_EXCEPTION);
	}
	
	/**
	 * Reads next token.
	 * @param clientEndpoint
	 * @return the token that was read
	 * @see Token
	 * @throws InterruptedException
	 */
	private Token readNextToken(ClientEndpoint clientEndpoint) throws InterruptedException {
		if(this.getNext(clientEndpoint) == ' ') {
			//System.out.println("Reading space");
			position++;
			return Token.SPACE;
		} else if(this.matchNextChars(clientEndpoint, '\r', '\n')) {
			//System.out.println("Reading EOL");
			this.position += 2;
			return Token.EOL; 
		} else {
			while(this.getNext(clientEndpoint) != ' ' && !this.matchNextChars(clientEndpoint, '\r', '\n')) {
				//System.out.println("Reading " + (char) this.getNext(clientEndpoint));
				position++;
			}
			return Token.WORD;
		}
	}
	
	/**
	 * Checks whether the next bytes are the same as the given ones.
	 * @param clientEndpoint
	 * @param must
	 * @return true if they match, false otherwise
	 * @throws InterruptedException
	 */
	private boolean matchNextChars(ClientEndpoint clientEndpoint, char... must) throws InterruptedException {
		for(int i = 0; i < must.length; i++) {
			if(position + i >= data.length) getNewData(clientEndpoint);
			if(must[i] != data[position + i]) return false;
		}
		return true;
	}
	
	/**
	 * Gets next byte, if there is no data left it waits for NetThread to write more.
	 * @param clientEndpoint
	 * @return
	 * @throws InterruptedException
	 */
	private byte getNext(ClientEndpoint clientEndpoint) throws InterruptedException {
		byte next;
		if(position >= data.length) getNewData(clientEndpoint);
		next = data[position];
		return next;
	}
	
	/**
	 * Gets new data from the clientEndpoint and concatenates to the already
	 * read/parsed data. If there is no new data it waits for the data to
	 * be written by the NetThread.
	 * @param clientEndpoint
	 * @throws InterruptedException
	 */
	private void getNewData(ClientEndpoint clientEndpoint) throws InterruptedException {
		byte[] newData = clientEndpoint.getData(id);
		byte[] temp = new byte[this.data.length + newData.length];
		System.arraycopy(this.data, 0, temp, 0, this.data.length);
		System.arraycopy(newData, 0, temp, this.data.length, newData.length);
		this.data = temp;
	}
	
	public void clearParser() {
		this.data = new byte[0];
		this.position = 0;
	}
}
