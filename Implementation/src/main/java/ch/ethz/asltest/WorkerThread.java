package ch.ethz.asltest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.operations.Operation;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.io.IOException;
import java.net.InetSocketAddress;


/**
 * The WorkerThread class implements the interface between the memcached servers and the middleware.
 * The implementation uses JavaNIO (blocking communication). The class itself implements the Java Runnable
 * interface, and hence works as a single thread in parallel with the rest of the program.
 * <p>
 * The main function of this class is to get new requests from the {@link MyMiddlewareQueue} and first
 * parse them and then process them.
 * </p>
 * <p>
 * In case the request is incomplete, the worker thread waits for NetThread to write the rest of the request,
 * in the bytebuffer.
 * </p>
 * 
 * @see MyMiddleware
 * @see WorkerThread
 * @author ardkastrati
 */
public class WorkerThread implements Runnable {
	
	private static final Logger logger = LogManager.getLogger(WorkerThread.class);
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	private static final int SERVER_BUFFER_CAPACITY = 82920;
	
	private int id;
	private BlockingQueue<ClientEndpoint> requestsQueue;
	private List<SocketChannel> socketsToMcServer;
	private ByteParser myParser;
	
	private ByteBuffer serverSendBuffer;
	private ByteBuffer serverRecvBuffer;
	private ByteBuffer clientSendBuffer;
	private static AtomicInteger roundRobinCtr; //Tells where to send next GET/MULTIGET ClientEndpoint
	
	WorkerThreadLogData myLogData;
	
	/**
	 * Constructs the WorkerThread. First it allocates for each memcached server a bytebuffer.
	 * Then it tries to connect with them.
	 * @param id the id of the worker thread assigned by the thread pool.
	 * @param requestsQueue the queue to read the requests.
	 * @param mcAddresses list of addresses (ip:port) of memcached servers with which the worker thread communicates.
	 * @param sharded the mode of the middleware.
	 */
	public WorkerThread(int id, BlockingQueue<ClientEndpoint> requestsQueue, List<String> mcAddresses, boolean sharded) {
		
		this.id = id;
		this.requestsQueue = requestsQueue;
		this.myParser = new ByteParser(id, sharded);
		this.myLogData = new WorkerThreadLogData();
		socketsToMcServer = new ArrayList<SocketChannel>(mcAddresses.size());
		WorkerThread.roundRobinCtr = new AtomicInteger(0);
		logger.debug(String.format("WorkerThread with id %d is allocating the buffers.", id));
		
		//Allocate the buffers for each worker thread
		serverSendBuffer = ByteBuffer.allocate(SERVER_BUFFER_CAPACITY);
		serverRecvBuffer = ByteBuffer.allocate(SERVER_BUFFER_CAPACITY);
		clientSendBuffer = ByteBuffer.allocate(SERVER_BUFFER_CAPACITY);
		
		logger.debug(String.format("WorkerThread with id %d allocated 1 sendbuffer and 1 recvBuffer.", id));
		
		for(int i = 0; i < mcAddresses.size(); i++) {
			try {
				String[] data = mcAddresses.get(i).split(":");
				logger.info(String.format("Worker Thread with id = %d is trying to connect to server with ip %s and port %d", 
						  id, data[0], Integer.parseInt(data[1])));
				
				SocketChannel sc = SocketChannel.open();
				InetSocketAddress addr = new InetSocketAddress(data[0], Integer.parseInt(data[1]));
				sc.connect(addr);
				socketsToMcServer.add(sc);

				logger.info(String.format("Worker Thread with id = %d is connected to server with ip %s and port %d", 
										  id, data[0], Integer.parseInt(data[1])));
	         } catch (IOException ioe) {
	        	 errorLogger.info("Couldn't establish connection: " + ioe.getMessage());
	        	 System.exit(-1);
	         }
		}
		logger.debug(String.format("Worker thread with id = %d is constructed.", id));
	}

	/**
	 * Starts the worker thread. The worker thread job is to get new requests from the queue
	 * and if there is one, parse it and process it (send the request to server, and send response 
	 * back to client).
	 */
	@Override
	public void run() {
		logger.debug(String.format("Worker thread with id %d is starting to process requests.", id));
		while(true) {
			ClientEndpoint req = null;
			try {
				req = requestsQueue.take();
				logger.debug(String.format("Worker thread with id %d took request with id %d", id, req.getId()));
				Operation operation = myParser.parseRequest(req);
				operation.sendRequest(id, serverSendBuffer, socketsToMcServer, roundRobinCtr, myLogData);
				operation.getResponse(id, serverRecvBuffer, clientSendBuffer, socketsToMcServer, req.getClient(), myLogData);
				
			} catch (InterruptedException e) {
				errorLogger.info("Interruption problem : " + e.getMessage());
			} catch (ParserException e) {
				myParser.clearParser();
				req.clearRequest();
				errorLogger.info("An unknown command was given, cleared the buffer and read until \\n" + e.getMessage());
			} catch (IOException e) {
				errorLogger.info("IO problem : " + e.getMessage());
			}
		}
	}
	
	public WorkerThreadLogData getLogData() {
		return this.myLogData;
	}
	
	public class WorkerThreadLogData {
		
		/*
		 * Logging data variables
		 */
		private int processedGETs = 0;
		private int processedSETs = 0;
		private int processedMULTIGETS = 0;
		private int cacheHits = 0;
		private int cacheMisses = 0;
		private long sumResponseTime = 0;
		private int[] buckets = new int[50000];
		
		public synchronized void addProcessedGETs(int processedGETs) {
			this.processedGETs += processedGETs;
		}
		
		public synchronized void addProcessedSETs(int processedSETs) {
			this.processedSETs += processedSETs;
		}
		
		public synchronized void addProcessedMULTIGETS(int processedMULTIGETS) {
			this.processedMULTIGETS += processedMULTIGETS;
		}
		
		public synchronized void addCacheHits(int cacheHits) {
			this.cacheHits += cacheHits;
		}
		
		public synchronized void addCacheMisses(int cacheMisses) {
			this.cacheMisses += cacheMisses;
		}
		
		public synchronized void addResponseTime(long responseTime) {
			this.addBucket(responseTime);
			this.sumResponseTime += responseTime;
		}
		
		public synchronized long[] getAndResetData() {
			long[] data = new long[6];
			data[0] = processedGETs;
			data[1] = processedSETs;
			data[2] = processedMULTIGETS;
			data[3] = cacheHits;
			data[4] = cacheMisses;
			data[5] = sumResponseTime;
			this.reset();
			return data;
		}
		
		private void reset()	{
			this.processedGETs = 0;
			this.processedSETs = 0;
			this.processedMULTIGETS = 0;
			this.cacheHits = 0;
			this.cacheMisses = 0;
			this.sumResponseTime = 0;
		}

		private void addBucket(long time) {
			int pos = (int) (time/100000); //100 microseconds
			if(pos >= 50000) pos = 49999; //5 seconds buckets.
			buckets[pos]++;
		}
		
		public synchronized int[] getBuckets() {
			return this.buckets;
		}
	}
}
