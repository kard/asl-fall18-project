package ch.ethz.asltest;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The <tt>MyMiddlewareQueue</tt> class represents the queue, which is one of the components of 
 * the <tt>MyMiddleware</tt>. The queue extends {@link ArrayBlockingQueue}, and accepts only
 * objects of type  {@link ClientEndpoint}. The class is responsible to add elements and remove elements,
 * concurrently from different threads. At the same time, the queue is responsible to instrument all statistics
 * relevant for the queue such as average queue time, average queue length, average queue waiting time
 * and any unexpected errors that might happen. 
 * @author ardkastrati
 * @see MyMiddleware
 * @see ClientEndpoint
 * @version 1.0
 */
public class MyMiddlewareQueue extends LinkedBlockingQueue<ClientEndpoint>{
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(MyMiddlewareQueue.class);
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	
	private MyMiddlewareQueueLogData myData;
	
	/**
	 * The constructor of the <tt>MyMiddlewareQueue</tt> class. 
	 * @param capacity the capacity of the queue (this represents the reserved capacity, it can be extended if
	 * needed, however a copy of the array will happen if it does happen)
	 */
	public MyMiddlewareQueue() {
		super();
		myData = new MyMiddlewareQueueLogData();
		logger.debug(String.format("MyMiddlewareQueue is constructed."));
	}
	
	/**
	 * Puts a clientEndpoint in the queue.
	 */
	@Override
	public void put(ClientEndpoint e) throws InterruptedException {
		e.setEnterQueueTime(System.nanoTime());
		super.put(e);
		logger.debug(String.format("MyMiddlewareQueue added the request with id %d.", e.getId()));
	}
	
	/**
	 * Takes a clientEndpoint from the queue.
	 */
	@Override
	public ClientEndpoint take() throws InterruptedException {
		ClientEndpoint e =  super.take();
		long timeInQueue = System.nanoTime() - e.getEnterQueueTime();
		myData.incrementNumOfElementsInAggregationTime();
		myData.addSumQueuedTime(timeInQueue);
		
		logger.debug(String.format("MyMiddlewareQueue removed the request with id %d.", e.getId()));
		return e;
	}
	
	public MyMiddlewareQueueLogData getLogData() {
		this.myData.setQueueSize(this.size());
		return this.myData;
	}
	
	public class MyMiddlewareQueueLogData {

		private long sumQueuedTime = 0;
		private int numOfElementsInAggregationTime = 0;
		private int queueSize =  0;
		
		public MyMiddlewareQueueLogData() {
			this.sumQueuedTime = 0;
			this.numOfElementsInAggregationTime = 0;
		}
		
		public synchronized void addSumQueuedTime(long sumQueuedTime) {
			this.sumQueuedTime += sumQueuedTime;
		}
		
		public synchronized void incrementNumOfElementsInAggregationTime() {
			this.numOfElementsInAggregationTime += 1;
		}
		
		public synchronized long[] getAndResetData() {
			long[] data = new long[3];
			data[0] = sumQueuedTime;
			data[1] = numOfElementsInAggregationTime;
			data[2] = queueSize;
			this.reset();
			return data;
		}
		
		public void reset() {
			this.sumQueuedTime = 0;
			this.numOfElementsInAggregationTime = 0;
			this.queueSize = 0;
		}
		
		public synchronized void setQueueSize(int queueSize) {
			this.queueSize = queueSize;
		}
	}
	
}
