package ch.ethz.asltest;

import java.util.*;
import org.apache.logging.log4j.*;

import ch.ethz.asltest.MyMiddlewareQueue.MyMiddlewareQueueLogData;
import ch.ethz.asltest.WorkerThread.WorkerThreadLogData;

import java.util.concurrent.BlockingQueue;

/**
 * The <tt>MyMiddleware</tt> class creates the Middleware. It creates all components of the middleware: 
 * <p>
 *  <ul>
 *   <li>{@link NetThread}</li>
 *   <li>{@link MyMiddlewareQueue}</li>
 *   <li>{@link ThreadPool}</li>
 *  </ul>
 * </p>
 * @author ardkastrati
 * @version 1.0
 */
public class MyMiddleware {
	
	private static final Logger logger = LogManager.getLogger("middlewareAgg");
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	
	private Thread netThread;
	private BlockingQueue<ClientEndpoint> requestsQueue;
	private ThreadPool workersThreadPool;
	
	/**
	 * The constructor of the <tt>MyMiddleware</tt> class. 
	 * @param myIp the IP where the middleware reads the requests from clients
	 * @param myPort the port where the middleware reads the requests from clients
	 * @param mcAddresses the addresses (myip:port) of the memcached servers
	 * @param numThreadsPTP numbers of threads in the thread pool
	 * @param readSharded sharded mode if true otherwise nonsharded mode
	 * @see MyMiddleware
	 */
	public MyMiddleware(String myIp, int myPort, List<String> mcAddresses, int numThreadsPTP, boolean readSharded) {
		logger.debug("MyMiddleware is starting...");
		requestsQueue = new MyMiddlewareQueue();
		netThread = new Thread(new NetThread(myIp, myPort, requestsQueue));
		workersThreadPool = new ThreadPool(mcAddresses, numThreadsPTP, readSharded, requestsQueue);
		
		TimerTask task = new TimerTask() {
			private int time = 0;
			private int buckets[] = new int[5000];
	        public void run() {
	        	WorkerThread[] theWorkers = workersThreadPool.getWorkers();
	        	if(theWorkers != null) {
	        		int processedGETs = 0;
	        		int processedSETs = 0;
	        		int processedMULTIGETS = 0;
	        		int cacheHits = 0;
	        		int cacheMisses = 0;
	        		long sumResponseTime = 0;
	        		for(WorkerThread worker : theWorkers) {
	        			long[] data = worker.getLogData().getAndResetData();
	        			processedGETs += data[0];
	        			processedSETs += data[1];
	        			processedMULTIGETS += data[2];
	        			cacheHits += data[3];
	        			cacheMisses += data[4];
	        			sumResponseTime += data[5];
	        		}
	        		long[] queueData =  ((MyMiddlewareQueue) requestsQueue).getLogData().getAndResetData();
	        		long sumQueuedTime = queueData[0];
	        		int numOfElementsInAggregationTime = (int) queueData[1];
	        		int queuSize = (int) queueData[2];
	    		
	        		//Log these
	        		StringBuilder toLog = new StringBuilder();
	        		toLog.append(time).append(",").append(processedGETs).append(",").append(processedSETs).
	    			  	append(",").append(processedMULTIGETS).append(",").append(cacheHits).
	    			  	append(",").append(cacheMisses).append(",").
	    			  	append((double) (sumResponseTime)/((cacheHits + cacheMisses) == 0 ? 1 : 1000*(cacheHits + cacheMisses))).
	    			  	append(",").append((double) sumQueuedTime/(numOfElementsInAggregationTime == 0 ? 1 : 1000*numOfElementsInAggregationTime)).
	    			  	append(",").append(queuSize);
	        		logger.info(toLog.toString());
	        	}
	        	time++;
	        }
	    };
	    Timer timer = new Timer("Timer");
		logger.info("Seconds,Processed-GETs,Processed-SETs,Processed-MULTIGETs,CacheHits,CacheMisses,Response-Time,Queue-Time,Queue-Size");
		
	    timer.schedule(task, 100, 1000);
		
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
				//Shut down properly!!
				timer.cancel();
				workersThreadPool.shutDown();
				System.out.println("Middleware was shut down!");
				} catch(Exception e) {
					errorLogger.info(e.getMessage());
				}
			}
		});
	} 
	

	/**
	 * Initiates all components of the middleware.
	 * @see MyMiddleware
	 */
	public void run() {
		logger.debug("MyMiddleware is starting NetThread and the WorkersThreads.");
		netThread.start();
		workersThreadPool.start();
	}
}
