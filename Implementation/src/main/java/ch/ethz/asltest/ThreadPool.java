package ch.ethz.asltest;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents the thread pool that is created in the middleware. ThreadPool is 
 * responsible to execute the worker threads and manage them if necessary.
 * @author ardkastrati
 *
 */
public class ThreadPool {
	
	private static final Logger logger = LogManager.getLogger(ThreadPool.class);
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	private List<String> mcAddresses;
	private int numThreadsPTP;
	boolean readSharded;
	private BlockingQueue<ClientEndpoint> requestsQueue;
	private ExecutorService exec;
	private WorkerThread[] myWorkers;
	private boolean workersInitialized = false;
	
	/**
	 * Constructs the ThreadPool.
	 * @param mcAddresses the addresses (list of ip:port) of memcached servers.
	 * @param numThreadsPTP the numbers of threads in the pool
	 * @param readSharded the mode of the worker threads.
	 * @param requestsQueue the queue to get new requests.
	 * @see ThreadPool
	 */
	public ThreadPool(List<String> mcAddresses, int numThreadsPTP, boolean readSharded, BlockingQueue<ClientEndpoint> requestsQueue) {
		this.mcAddresses = mcAddresses;
		this.numThreadsPTP = numThreadsPTP;
		this.readSharded = readSharded;
		this.requestsQueue = requestsQueue;
		this.exec = Executors.newFixedThreadPool(numThreadsPTP);
		logger.debug(String.format("The thread pool with size %d and type %s is constructed.", numThreadsPTP, readSharded ? "sharded" : "non-sharded"));
	}
	
	/**
	 * Starts the worker threads, which are used to process the requests.
	 */
	public void start() {
		logger.debug("Thread pool is starting the worker threads.");
		myWorkers = new WorkerThread[numThreadsPTP];
		for(int i = 0; i < numThreadsPTP; i++) {
			Runnable workerThread = new WorkerThread(i, requestsQueue, mcAddresses, readSharded);
			exec.execute(workerThread);
			myWorkers[i] = (WorkerThread) workerThread;
		}
		workersInitialized = true;
		logger.debug("Worker threads are up.");
	}
	
	public WorkerThread[] getWorkers() {
		if(workersInitialized) return this.myWorkers;
		else return null;
	}
	
	public void shutDown() {
		int[] sumBuckets = new int[50000];
		for(WorkerThread worker : myWorkers) {
			int[] next = worker.getLogData().getBuckets();
			Arrays.setAll(sumBuckets, i -> sumBuckets[i] + next[i]);
		}
		
		StringBuilder toLog = new StringBuilder();
		for(int a : sumBuckets) {
			toLog.append(a).append(" ");
		}
		PrintWriter writer;
		try {
			writer = new PrintWriter("logs/histogram.log", "UTF-8");
			writer.println(toLog.toString());
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
