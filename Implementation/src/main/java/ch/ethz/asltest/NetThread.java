package ch.ethz.asltest;

import org.apache.logging.log4j.*;

import java.net.InetSocketAddress;
import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;
import java.util.concurrent.BlockingQueue;


/**
 * The NetThread class implements the interface between the clients to the middleware. The implementation
 * uses Java NIO (non-blocking communication). The class itself implements the Java Runnable interface,
 * and hence works as a single thread in parallel with the rest of the program.
 * <p>
 * The main function of this class is to accept connections/read the requests and put them on the queue.
 * </p>
 * <p>
 * The netThread only reads the requests from the client socket channel to allocated ByteBuffers, however 
 * the parsing will happen will be executed by worker threads, to avoid the bottleneck at the NetThread.
 * </p>
 * In addition, per client only one ByteBuffer will be allocated only once, and it will be reused during 
 * the whole time.
 * 
 * @see MyMiddleware
 * @see WorkerThread
 * @author ardkastrati
 *
 */
public class NetThread implements Runnable {
	
	private static final Logger logger = LogManager.getLogger(NetThread.class);
	private static final Logger errorlogger = LogManager.getLogger("middlewareError");
	private static final String ERROR_INFO = "ERROR IN NET THREAD";
	
	private String myIp;
	private int myPort;
	private BlockingQueue<ClientEndpoint> requestsQueue;
	
	
	/**
	 * The constructor of the NetThread. Constructs the NetThread that listens the requests on the 
	 * specified ip and port.
	 * @param myIp the ip address the NetThread listens.
	 * @param myPort the port the NetThread listens.
	 * @param requestsQueue the queue where the requests are added
	 */
	public NetThread(String myIp, int myPort, BlockingQueue<ClientEndpoint> requestsQueue) {
		this.myIp = myIp;
		this.myPort = myPort;
		this.requestsQueue = requestsQueue;
		logger.debug(String.format("NetThread with ip %s and port %d is constructed.", myIp, myPort));
	}

	/**
	 * Starts the netThread. NetThread uses {@link Selector} from Java NIO to communicate in non-blocking
	 * manner with a possibly high number of clients. This achieves a highly scalable middleware.
	 */
	@Override
	public void run() {
		logger.debug(String.format("Starting the NetThread with ip = %s, and port = %d.", myIp, myPort));
		try {
			
			Selector selector = Selector.open();
			ServerSocketChannel ssc = ServerSocketChannel.open();
			ssc.bind(new InetSocketAddress(myPort), 250); //I HATE BACKLOG (it took me 8 hours to debug)
			ssc.configureBlocking(false);
			ssc.register(selector, SelectionKey.OP_ACCEPT);
			logger.debug("NetThread is ready.");
			int id = 0;
			while(true) {
				logger.debug("NetThread is waiting for next request...");
				int numReadyChannels = selector.select();
				if (numReadyChannels == 0) continue;
				
				Set<SelectionKey> selectedKeys = selector.selectedKeys();
				Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
				
				while (keyIterator.hasNext()) {
				      SelectionKey key = keyIterator.next();
				      if (key.isValid() && key.isAcceptable()) { 
				         acceptConnection(key, selector, id);
				         id++;
				      } else if (key.isValid() && key.isReadable()) {
				    	 readRequest(key);
				      }
				      keyIterator.remove();
				}
			}
		} catch (IOException | InterruptedException e) {
			errorlogger.info(ERROR_INFO + e.getMessage());
			System.exit(-1);
		}
	}
	
	/**
	 * Accepts a new connection from a new client. When a client connection is accepted, 
	 * each client will get a {@link ClientEndpoint} object attached to this connection, which is reused
	 * every time when a new request comes from the client. The same object is reused for different 
	 * reasons. First, it avoids reallocation of the buffers and the copying of data from
	 * client buffer to other arrays so it can be processed from worker threads. Second, it makes possible to remove 
	 * the burden of net thread of parsing the request before sending to the client. With this object,
	 * the net Thread can write new data without parsing it, and the worker threads on the other hand,
	 * can see these changes (if some worker thread is already working in it). 
	 * @param key
	 * @param selector
	 * @param id
	 * @see NetThread#readRequest(SelectionKey)
	 * @throws IOException
	 */
	private void acceptConnection(SelectionKey key, Selector selector, int id) throws IOException {
		 logger.debug("NetThread found a new CONNECTION request");
		 
         // A connection was accepted by a ServerSocketChannel.
         ServerSocketChannel server = (ServerSocketChannel) key.channel();
         SocketChannel client = server.accept();
         if(client == null) return;
         client.configureBlocking(false);
         ClientEndpoint clientRequest = new ClientEndpoint(id, client);
         client.register(selector, SelectionKey.OP_READ, clientRequest);
         
         logger.debug("NetThread accepted new CONNECTION request.");
	}
	
	/**
	 * Reads the request from the client socket channel to the client bytebuffer. 
	 * No parsing of the data happens here, in order to remove the burden of the NetThread.
	 * In case the request from the client is new, the bytebuffer of {@link ClientEndpoint} object,
	 * will be put in the queue. If the {@link ClientEndpoint} object is already on the queue, or
	 * already being parsed by some worker thread on the other side (that means another part of
	 * the request came into the socket channel), then only the bytebuffer will be updated.
	 * @param key
	 * @throws InterruptedException
	 * @throws IOException
	 */
	private int readRequest(SelectionKey key) throws InterruptedException, IOException {
		logger.debug("NetThread found a new request.");
		ClientEndpoint req = (ClientEndpoint) key.attachment();
		int read;
        // A socket channel is ready for reading.
		if(req.getState() == ClientEndpoint.State.READY) {
			logger.debug("NetThread is reading the request for the first time.");
			read = req.readRequest();
			if(read == -1) key.cancel();
			else requestsQueue.put(req);
		} else if(req.getState() == ClientEndpoint.State.INCOMING_REQUEST) {
			logger.debug("NetThread is reading the second part of the initial request.");
			read = req.readRequest();
			if(read == -1) key.cancel();
		} else {
			logger.debug("NetThread is trying to read a request from a client, before finishing the last request.");
			read = -1;
		}
		return read;
	}
}
