package ch.ethz.asltest.operations;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.WorkerThread;
import ch.ethz.asltest.WorkerThread.WorkerThreadLogData;

/**
 * The multiget <tt>value</tt> operation gets the values from different keys in the data storage at 
 * the memcached servers.
 * @see WorkerThread
 * @see Operation
 */
public class MultiGetOperation extends Operation {
	private static final Logger logger = LogManager.getLogger();
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	
	byte[] request;
	private int myServer;
	private WorkerThreadLogData logData;
	private long time;
	private int nunOfGets;
	
	/**
	 * Constructs the MULTIGET operation with the given (already verified during parsing) request.
	 * @param socketToMcServer
	 * @param key
	 */
	
	public MultiGetOperation(byte[] request, int numOfGets) {
		this.request = request;
		logger.debug(String.format("MULTIGET operation with is constructed."));
	}
	
	/**
     * <p>
     * It processes the MultiGet command through the provided server socket channel and the key.
     * Then it returns the response to the client.
	 * @throws IOException 
     * @see Operation
     * @throws CommandException if an error occurs during the GET operation.
     */

	@Override
	public void sendRequest(int workerThreadId, ByteBuffer serverSendBuffer,
						    List<SocketChannel> socketsToMcServer,
						    AtomicInteger roundRobinCtr, WorkerThreadLogData myLogData) throws IOException {
		
		logger.debug(String.format("WorkerThread with id %d is sending the MULTIGET request.", workerThreadId));
		myServer = roundRobinCtr.getAndUpdate(x -> increment(x, 1, socketsToMcServer.size()));
		
		logger.debug(String.format("WorkerThread with id %d is sending the MULTIGET request: to server %d.", workerThreadId, myServer));
		serverSendBuffer.put(request);
		serverSendBuffer.flip();
		
		this.sendDataToChannel(socketsToMcServer.get(myServer), serverSendBuffer);
		this.time = System.nanoTime();
		serverSendBuffer.clear();
	}
	

	@Override
	public void getResponse(int workerThreadId, ByteBuffer serverRecvBuffer, ByteBuffer clientSendBuffer,
			 				List<SocketChannel> socketsToMcServer,  
			 				SocketChannel clientChannel, WorkerThreadLogData myLogData) throws IOException {
		
		logger.debug(String.format("WorkerThread with id %d is receiving the answer for MULTIGET request: from server %d ", workerThreadId, myServer));
		this.recvDataFromChannel(socketsToMcServer.get(myServer), serverRecvBuffer, END);
		myLogData.addResponseTime(System.nanoTime() - time);
		int n = countCacheMisses(serverRecvBuffer);
		myLogData.addProcessedGETs(nunOfGets);
		myLogData.addProcessedMULTIGETS(1);
		myLogData.addCacheMisses(n);
		myLogData.addCacheHits(1);
		
		logger.debug(String.format("WorkerThread with id %d received the answer for MULTIGET request: from server %d ", workerThreadId, myServer));
		serverRecvBuffer.flip();
		logger.debug(String.format("WorkerThread with id %d is sending the response back to client.", workerThreadId));
		this.sendDataToChannel(clientChannel, serverRecvBuffer);
		serverRecvBuffer.clear();
	}
	
	private int countCacheMisses(ByteBuffer buffer) {
		//Look for the first \r\n
		int n = 0;
		for(int i = 0; i < buffer.position() - 2; i++) {
			if(buffer.get(i) == '\n') {
					if(i + 1 < buffer.position() && buffer.get(i+1) == '\r') {
						i++;
					}
			}
		}
		return n;
	}



}
