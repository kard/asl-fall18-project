package ch.ethz.asltest.operations;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.WorkerThread;
import ch.ethz.asltest.WorkerThread.WorkerThreadLogData;

/**
 * The set <tt>value</tt> operation set the respective key-values in the
 * data storage at the memcached servers.
 * @see WorkerThread
 * @see Operation
 */
public class SetOperation extends Operation {
	private static final Logger logger = LogManager.getLogger();
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	byte[] request;
	private long time;
	
	/**
	 * Constructs the SET operation with the give (already verfied during the parsing) request.
	 * @param socketToMcServer
	 * @param key
	 */
	public SetOperation(byte[] request) {
		this.request = request;
		logger.debug(String.format("SET operation is constructed."));
	}
	
	/**
     * It processes the SET command through the provided server socket channels and the key-values.
	 * @throws IOException 
     * @see Operation
     * @throws CommandException if an error occurs during the GET operation.
     */
	@Override
	public void sendRequest(int workerThreadId, ByteBuffer serverSendBuffer, 
					  		List<SocketChannel> socketsToMcServer,
					  		AtomicInteger roundRobinCtr, WorkerThreadLogData myLogData) throws IOException {
		
		//System.out.println("The request is: " + request.toString());
		logger.debug(String.format("WorkerThread with id %d is sending the SET request.", workerThreadId));
		serverSendBuffer.put(this.request);
		serverSendBuffer.flip();
		
		for(int i = 0; i < socketsToMcServer.size(); i++) {
			logger.debug(String.format("WorkerThread with id %d is sending the SET request: to server %d", workerThreadId, i));
			this.sendDataToChannel(socketsToMcServer.get(i), serverSendBuffer);
			serverSendBuffer.rewind();
		}
		time = System.nanoTime();
		serverSendBuffer.clear();
	}
	
	/**
	 * Assembles the responsed from each server and sends the response back to the client as specified
	 * in the assignment. (if there is one error, relay it back to the client).
	 */
	@Override
	public void getResponse(int workerThreadId, ByteBuffer serverRecvBuffer, ByteBuffer clientSendBuffer,
							List<SocketChannel> socketsToMcServer,
							SocketChannel clientChannel, WorkerThreadLogData myLogData) throws IOException {
		
		boolean foundError = false;
		clientSendBuffer.put(STORED);
		clientSendBuffer.flip();
		for(int i = 0; i < socketsToMcServer.size(); i++) {
			logger.debug(String.format("WorkerThread with id %d is receiving the answer for SET request: from server %d ", workerThreadId, i));
			this.recvDataFromChannel(socketsToMcServer.get(i), serverRecvBuffer, CRLF);
			
			myLogData.addResponseTime(System.nanoTime() - time);
			myLogData.addProcessedSETs(1);
			myLogData.addCacheHits(1);

			logger.debug(String.format("WorkerThread with id %d is sending the response back to client.", workerThreadId));
			//Here need to check the answers
			serverRecvBuffer.flip();
			if(!foundError && !serverRecvBuffer.equals(clientSendBuffer)) {
				logger.debug("An error SET response found!");
				foundError = true;
				clientSendBuffer.clear();
				clientSendBuffer.put(serverRecvBuffer);
				clientSendBuffer.flip();
			}
			serverRecvBuffer.clear();
		}	
		this.sendDataToChannel(clientChannel, clientSendBuffer);
		clientSendBuffer.clear();
	}	
	
}
