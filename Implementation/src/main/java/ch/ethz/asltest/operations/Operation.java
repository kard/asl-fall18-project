package ch.ethz.asltest.operations;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import ch.ethz.asltest.WorkerThread.WorkerThreadLogData;

/**
 * The <tt>Operation</tt> interface abstracts the operations in the middleware.
 * Depending on the operation, some operations have more parameters while
 * other operations only one. <br/>
 * The following list contains the supported commands and information about their parameters:
 * 
 * <p align="center">
 * <style> td {text-align: center;} </style>
 * <table border="1" style="border-collapse:collapse;border-color:gray;">
 * <tr><th>Operation name</th><th>Parameter type</th></tr>
 * Each operation must implement the process() function, which will be executed from the 
 * one of the threads in the thread pool.
 * 
 * <tr><td>GET</td><td>{@link byte[]} request</td></tr>
 * <tr><td>SET</td><td>{@link byte[]} request</td> </tr>
 * <tr><td>MULTIGET</td><td>{@link byte[]} request (Multiple Keys)</td></tr>
 * <tr><td>MULTIGET(sharded)</td><td>{@link byte[]} request and  {@link List<Integer>} whiteSpaces</td></tr>
 * </table>
 * </p>
 * 
 * @author ardkastrati
 * @version 1.0
 * @see {@link #GET}, {@link #MULTIGET}, {@link #SET}
 */
public abstract class Operation {
	
	public static Charset charset = Charset.forName("UTF-8");
	public static CharsetEncoder encoder = charset.newEncoder();
	public static CharsetDecoder decoder = charset.newDecoder();
	
	protected static final byte[] END = "END\r\n".getBytes();
	protected static final byte[] STORED = "STORED\r\n".getBytes();
	protected static final byte[] ERROR = "ERROR\r\n".getBytes();
	protected static final byte[] SERVER_ERROR = "SERVER_ERROR".getBytes();
	protected static final byte[] CLIENT_ERROR = "CLIENT_ERROR".getBytes();
	protected static final byte[] CRLF =  "\r\n".getBytes();
	
	protected int time_started = -1;
	protected int time_finished = -1;
	protected int time_server_response = -1;
	
	protected void sendDataToChannel(SocketChannel channel, ByteBuffer buffer) throws IOException {
		channel.write(buffer);
	}
	
	protected void recvDataFromChannel(SocketChannel channel, ByteBuffer buffer, byte[] endPattern) throws IOException {
		boolean readResponse = false;
		while(!readResponse) {
			channel.read(buffer);
			buffer.position(buffer.position() - endPattern.length);
			boolean foundEnd = true;
			for(int i = 0; i < endPattern.length; i++) foundEnd &= buffer.get() == endPattern[i];
			readResponse = foundEnd;
		}
	}
	
	protected int increment(int x , int y, int z) {
		return (x + y) % z;
	}
	
	public abstract void sendRequest(int workerThreadId, ByteBuffer serverSendBuffer,
								 	 List<SocketChannel> socketsToMcServer, 
								 	 AtomicInteger roundRobin, WorkerThreadLogData myLogData) throws IOException;
	
	public abstract void getResponse(int workerThreadId, ByteBuffer serverSendBuffers, ByteBuffer clientSendBuffer,
			 						 List<SocketChannel> socketsToMcServer, 
									 SocketChannel clientChannel, WorkerThreadLogData myLogData) throws IOException;
	
	
}
