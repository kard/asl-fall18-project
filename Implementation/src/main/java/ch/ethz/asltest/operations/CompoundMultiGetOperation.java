package ch.ethz.asltest.operations;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.WorkerThread;
import ch.ethz.asltest.WorkerThread.WorkerThreadLogData;

/**
 * The middleware in a sharded mode, will split a MultiGet operation into a set of smaller multi-GET 
 * requests. Hence the CompoundMultiGetOperation represent a MultiGetOperation in a sharded mode.
 * 
 * The multiget operation gets the values from different keys in the data storage at 
 * the memcached servers.
 * @see WorkerThread
 * @see Operation
 */
public class CompoundMultiGetOperation extends Operation{
	private static final Logger logger = LogManager.getLogger();
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	
	private static final byte[] GET = "get".getBytes();
	private static final byte[] EOL = "\r\n".getBytes();
	private byte[] request;
	private List<Integer> whiteSpaces;
	private int myFirstServer;
	private int numOfMultigets;
	private int numOfKeys;
	private long time;
	
	/**
	 * Constructs the MULTIGET operation in a sharded mode with the given MULTIGET request 
	 * and the whitespaces where the keys are separated (Found during the parsing).
	 * @param socketToMcServer
	 * @param key
	 */
	public CompoundMultiGetOperation(byte[] request, List<Integer> whiteSpaces) {
		this.request = request;
		this.whiteSpaces = whiteSpaces;
		this.numOfKeys = whiteSpaces.size() - 1;
		logger.debug("CompoundMULTIGET operation is constructed.");
	}

	/**
	 * Sends the request to the servers as specified in the assignment. It splits the multi-GET into a set of smaller 
	 * multi-GET requests, one request for each server. A round robin load balancing is implemented. In order to choose
	 * the next server where to send the multi-GET, a global round robin is used.
	 */
	@Override
	public void sendRequest(int workerThreadId, ByteBuffer serverSendBuffer,
							List<SocketChannel> socketsToMcServer, 
							AtomicInteger roundRobinCtr, WorkerThreadLogData myLogData) throws IOException {
		
		logger.debug(String.format("WorkerThread with id %d is sending the MULTIGET(sharded) request.", workerThreadId));

		int q = numOfKeys / socketsToMcServer.size();
		int r = numOfKeys % socketsToMcServer.size();
		this.numOfMultigets = (q == 0) ? r : socketsToMcServer.size();
		
		myFirstServer = roundRobinCtr.getAndUpdate(x -> increment(x, numOfMultigets, socketsToMcServer.size()));
		
		logger.debug(String.format("MULTIGET (sharded) with q %d and r %d!", q, r));
		int counter = myFirstServer;
		for(int i = 0; i < r; i++) {
			//These servers get q+1 keys
			serverSendBuffer.put(GET);
			serverSendBuffer.put(request, whiteSpaces.get((q+1)*i), whiteSpaces.get((q+1)*(i+1)) - whiteSpaces.get((q+1)*i));
			serverSendBuffer.put(EOL);
			serverSendBuffer.flip();
			logger.debug(String.format("WorkerThread with id %d is sending the MULTIGET request: to server %d with %d keys ", workerThreadId, counter, q+1));
			this.sendDataToChannel(socketsToMcServer.get(counter), serverSendBuffer);
			serverSendBuffer.clear();
			counter = (counter + 1) % socketsToMcServer.size();
		}
		
		for(int i = r; i < this.numOfMultigets; i++) {
			//These servers get q keys
			serverSendBuffer.put(GET);
			serverSendBuffer.put(request, whiteSpaces.get((q+1)*r + q*(i-r)), whiteSpaces.get((q+1)*r + q*(i-r+1)) - whiteSpaces.get((q+1)*r + q*(i-r)));
			serverSendBuffer.put(EOL);
			serverSendBuffer.flip();
			logger.debug(String.format("WorkerThread with id %d is sending the MULTIGET request: to server %d with %d keys ", workerThreadId, counter, q));
			
			this.sendDataToChannel(socketsToMcServer.get(counter), serverSendBuffer);
			serverSendBuffer.clear();
			counter = (counter + 1) % socketsToMcServer.size();
		}
		time = System.nanoTime();
	}

	/**
	 * It assembles the responses from the servers and sends the response back to client.
	 * back to client.
	 */
	@Override
	public void getResponse(int workerThreadId, ByteBuffer serverRecvBuffer, ByteBuffer clientSendBuffer,
			 				List<SocketChannel> socketsToMcServer,  
			 				SocketChannel clientChannel, WorkerThreadLogData myLogData) throws IOException {
		
		int counter = myFirstServer;
		for(int i = 0; i < this.numOfMultigets; i++) {
			logger.debug(String.format("WorkerThread with id %d is receiving the answer for MULTIGET request: from server %d ", workerThreadId, counter));
			myLogData.addProcessedMULTIGETS(1);
			this.recvDataFromChannel(socketsToMcServer.get(counter), serverRecvBuffer, END);
			if(i < this.numOfMultigets - 1) serverRecvBuffer.position(serverRecvBuffer.position() - 5);
			counter = (counter + 1) % socketsToMcServer.size();
		}
		myLogData.addResponseTime(System.nanoTime() - time);
		int n = countCacheMisses(serverRecvBuffer);
		myLogData.addProcessedMULTIGETS(1);
		myLogData.addCacheMisses(n);
		myLogData.addCacheHits(1);
		
		serverRecvBuffer.flip();
		logger.debug(String.format("WorkerThread with id %d is sending the response back to client.", workerThreadId));
		this.sendDataToChannel(clientChannel, serverRecvBuffer);
		serverRecvBuffer.clear();
	}
	
	private int countCacheMisses(ByteBuffer buffer) {
		//Look for the first \r\n
		int n = 0;
		for(int i = 0; i < buffer.position() - 2; i++) {
			if(buffer.get(i) == '\n') {
					if(i + 1 < buffer.position() && buffer.get(i+1) == '\r') {
						i++;
					}
			}
		}
		return n;
	}
	
	
}
