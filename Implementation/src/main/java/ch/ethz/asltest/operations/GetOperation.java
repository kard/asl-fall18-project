package ch.ethz.asltest.operations;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.CharacterCodingException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.WorkerThread;
import ch.ethz.asltest.WorkerThread.WorkerThreadLogData;

/**
 * The get <tt>value</tt> operation gets the respective value stored in the
 * key value in the data storage at the memcached servers.
 * @see WorkerThread
 * @see Operation
 */
public class GetOperation extends Operation {
	private static final Logger logger = LogManager.getLogger();
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	
	private byte[] request;
	private int myServer;
	private long time = -1;
	
	/**
	 * Constructs the GET operation with the (already verified during the parsing) request.
	 * @param socketToMcServer
	 * @param key
	 */
	public GetOperation(byte[] request) {
		this.request = request;
		logger.debug(String.format("GET operation is constructed."));
	}
	
	/**
     * <p>
     * It processes the GET command through the server socket channel. 
     * A round robin load balancing is implemented. In order to choose the next server 
     * where to send the GET request, a global round robin is used.
	 * @throws IOException 
	 * @throws CharacterCodingException 
     * @see Operation
     * @throws CommandException if an error occurs during the GET operation.
     */
	@Override
	public void sendRequest(int workerThreadId, ByteBuffer serverSendBuffer,
							List<SocketChannel> socketsToMcServer, 
							AtomicInteger roundRobinCtr, WorkerThreadLogData myLogData) throws IOException {
		logger.debug(String.format("WorkerThread with id %d is sending the GET request.", workerThreadId));
		myServer = roundRobinCtr.getAndUpdate(x -> increment(x, 1, socketsToMcServer.size()));
		logger.debug(String.format("WorkerThread with id %d is sending the GET request: to server %d.", workerThreadId, myServer));
		serverSendBuffer.put(request);
		serverSendBuffer.flip();
		
		this.sendDataToChannel(socketsToMcServer.get(myServer), serverSendBuffer);
		this.time = System.nanoTime();
		serverSendBuffer.clear();
	}
	
	/**
	 * Relays the response back to client.
	 */
	@Override
	public void getResponse(int workerThreadId, ByteBuffer serverRecvBuffer, ByteBuffer clientSendBuffer,
							 List<SocketChannel> socketsToMcServer,  
							 SocketChannel clientChannel, WorkerThreadLogData myLogData) throws IOException {
		
		logger.debug(String.format("WorkerThread with id %d is receiving the answer for GET request: from server %d ", workerThreadId, myServer));
		this.recvDataFromChannel(socketsToMcServer.get(myServer), serverRecvBuffer, END);
		
		myLogData.addResponseTime(System.nanoTime() - time);
		myLogData.addProcessedGETs(1);
		if(checkCacheMiss(serverRecvBuffer)) myLogData.addCacheMisses(1);
		else myLogData.addCacheHits(1);
		
		logger.debug(String.format("WorkerThread with id %d received the answer for GET request: from server %d ", workerThreadId, myServer));
		serverRecvBuffer.flip();
		logger.debug(String.format("WorkerThread with id %d is sending the response back to client.", workerThreadId));
		this.sendDataToChannel(clientChannel, serverRecvBuffer);
		serverRecvBuffer.clear();
	}
	
	private boolean checkCacheMiss(ByteBuffer buffer) {
		//Look for the first \r\n
		int n = 0;
		for(int i = 0; i < buffer.position() - 2; i++) {
			if(buffer.get(i) == '\n') {
					if(buffer.get(i+1) == '\r') {
						return true;
					} else {
						return false;
					}
			}
		}
		return false;
	}

}
