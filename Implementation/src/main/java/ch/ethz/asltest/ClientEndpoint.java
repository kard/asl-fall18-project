package ch.ethz.asltest;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.concurrent.BlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ch.ethz.asltest.ClientEndpoint.State;
/**
 * The <tt>ClientEndpoint</tt> is the entity that will be put on the queue, which will be later parsed into an operation
 * and processed by the WorkThread. In order to be more efficient, and avoid the bottleneck that can occur 
 * in the NetThread, no processing/parsing of the request from the client happens in the NetThread.
 * <p>
 * The client endpoint object is responsible to allocate the buffer, where the request of the client is read,
 * it has a reference of the client socket channel, a state and an id (for instrumentation).
 * To avoid reallocation of the ByteBuffers, recreation of the object for each request, and since the client
 * are assumed to be blocking each client has only one client endpoint assigned to him, and the same object
 * is reused during the whole time. Thus the client endpoint is created only once, when a connection
 * between the client and the middleware is created.
 * </p>
 * <p>
 * 	The client endpoint can be in one of the following states.
 *  <ul>
 *   <li>{@link State#READY}</li>
 *   <li>{@link State#INCOMING_REQUEST}</li>
 *  </ul>
 * </p>
 * <p>
 * However, the netThread is still responsible to read the data from the socket channel and write it to
 * the (only once allocated) ByteBuffer of the client, because the Java NIO doesn't offer a better alternative.
 * If we don't read the data immediately, the {@link Selector#select()} will not wait for new data, 
 * but will always return because there is already unprocessed data. Hence, we would be forced to do busy waiting,
 * in NetThread, until some worker threads reads it. Otherwise, the complexity of the program would increase a lot.
 * </p>
 * 
 * @author ardkastrati
 * @see MyMiddleware
 * @see NetThread
 * @see WorkerThread
 *
 */
public class ClientEndpoint {
	

	private static final Logger logger = LogManager.getLogger(ClientEndpoint.class);
	private static final Logger errorLogger = LogManager.getLogger("middlewareError");
	private static final int CLIENT_BUFFER_CAPACITY = 82920;
	
	/**
	 * The <tt>State</tt> enum represents the states the request object can be.
	 * @author ardkastrati
	 * @see ClientEndpoint
	 *
	 */
	public enum State {
		/**
		 * Represents the state when the clientEndpoint is ready for new requests, which can be 
		 * accepted by the net Thread from the client. When the state is READY, it means the request,
		 * is not in the queue, or already being processed by some worker thread.
		 */
		READY,
		/**
		 * Represents the state when the some data is already read from the client as part of the new request.
		 * The object at this state, might be either in the queue, or already being parsed by the worker thread.
		 */
		INCOMING_REQUEST,
	}
	
	private SocketChannel clientChannel;
	private ByteBuffer byteBuffer;
	private State currentState;
	private int id;
	
	private long enterQueueTime;
	
	/**
	 * Constructs the clientEndpoint.
	 * @param id the id of the clientEndpoint
	 * @param clientChannel
	 * @see ClientEndpoint
	 */
	public ClientEndpoint(int id, SocketChannel clientChannel) {
		this.clientChannel = clientChannel;
		this.byteBuffer = ByteBuffer.allocate(CLIENT_BUFFER_CAPACITY);
		this.currentState = State.READY;
		this.enterQueueTime = -1;
		this.id = id;
		logger.debug(String.format("ClientEndpoint with id %d is constructed.", id));
	}
	/**
	 * Gets the data from the bytebuffer. The method is synchronized, 
	 * since the ByteBuffers are not thread safe (in case the netThread is already reading
	 * some other part of the request from the socket channel).
	 * @param workThreadId
	 * @return
	 * @throws InterruptedException
	 */
	public synchronized byte[] getData(int workThreadId) throws InterruptedException {
		logger.debug(String.format("Worker thread with id %d is reading current data...", workThreadId));
		byteBuffer.flip();
		while(!this.byteBuffer.hasRemaining()) {
			byteBuffer.clear();
			this.wait();
			byteBuffer.flip();
		}
		byte[] data = new byte[this.byteBuffer.remaining()];
		this.byteBuffer.get(data);
		byteBuffer.clear();
		return data;
	}
	
	/**
	 * Reads the request from the client socket channel to the Bytebuffer. This method is
	 * called by the NetThread and synchronized (in case some worker thread is already  
	 * reading the data from the bytebuffer)
	 * @throws IOException
	 */
	public synchronized int readRequest() throws IOException {
		logger.debug(String.format("ClientEndpoint with id %d is being read.", id));
		int read = this.clientChannel.read(this.byteBuffer);
		this.currentState = State.INCOMING_REQUEST;
		this.notifyAll();
		return read;
	}
	
	/**
	 * Clears the request. After this method is called the client Endpoint is ready to be
	 * reused for a new request from the client side.
	 */
	public void clearRequest() {
		this.enterQueueTime = -1;
		this.currentState = State.READY;
		this.byteBuffer.clear();
	}
	

	/**
	 * Returns the state of the clientEndpoint.
	 * @see State
	 * @return the state of the request
	 */
	public State getState() {
		return this.currentState;
	}

	/**
	 * Sets the enter time of the request in the queue.
	 * @param time the time the request enteret the queue.
	 */
	public void setEnterQueueTime(long time) {
		this.enterQueueTime = time;
	}
	
	/**
	 * Gets the enter time of the request the queue.
	 * @return the time the request entered the queue, -1 otherwise
	 */
	public long getEnterQueueTime() {
		return this.enterQueueTime;
	}

	/**
	 * Returns the client socket channel.
	 * @return
	 */
	public SocketChannel getClient() {
		return this.clientChannel;
	}
	
	/**
	 * Returns the id of this request.
	 * @return
	 */
	public int getId() {
		return this.id;
	}


	
	
	
	
}
