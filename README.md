# Advanced Systems Lab

This is the project repository for the Advanced Systems Lab 2018. 


## Overview

In the repository, you can find the report and Implementation folder, where code, scripts, experimental data can be found.

## Start the program
```console
foo@bar:~$ cd Implementation
foo@bar:~$ ant jar
foo@bar:~$ java -jar dist/middleware-kard.jar ... (args)
```

## Start the experiments
    - Unzip the experiments.zip
    - Via scp send azure/client_machine1/* to VM1, azure/client_machine2/* to VM2, and so on
    - Run sh start x.y 
    Example:
    foo@bar:~$ sh start.sh 2.1

## Plot the data
    - Open the experiments folder as a project in PyCharm
    - Run the plot.py of the chapter that you want to plot the data.


## Report

The report consists of 35 + 2 pages (first page the title and the last page references). 

## Implementation
Under the implementation folder, the following files and subdirectories can be found:
- src/: The implementation of the middleware (in Java)
- build.xml: ANT Build File (includes log4j)
- build/ and dist/: Directories used by ANT
- experiments.zip: All scripts (plotting, starting experiments on Azure, downloading the data) are separated by chapters.

### Details about the experiments folder
    
- **azure/**
    -In this directory, you can find all the files and folders that should be loaded in each virtual machine on Azure. In order to start the experiments, the folders should be uploaded to the machines (e.g. with scp)
- **chapterX/chapterX.Y/**
    - For each subchapter there exists a path as given above (except first chapter). This directory has the folder raw_data/, where all the data from that type of experiment from each machine is collected. If a subchapter involves more experiments they are then separated again through subdirectories. 
    - In case plotting the data was needed for that subchapter, a file called plot.py can be found. This file plots the data directly from the raw_data (there are exceptions, where data are first processed. In that case a processed_data/ directory can be found there as well). 
    - In order to start the experiments for that subchapter, a bash script called start.sh is always there. 
    
- **utils_bash/ and utils_python/**
    - Some functions are needed in every chapter while running the data. Hence, these functions are implemented here and called from plot.py of each chapter. It is important however, where you start the plot.py because it can happen that the program doesn't find the imported functions (In my case I used the pyCharm IDE)
    
### Data recorded in the experiments
- In every experiment, the client-stats-0-X-Y.csv are recorded for each virtual client, the memtier.out file of the benchmark, and the same data in json format is stored at the file json.out
- For the middleware, in each experiment under logs/ directory, the logged files are stored (see the report what is logged)
- For each machine, the data recorded with dstat is stored under the directory data/ for each experiment.
    



